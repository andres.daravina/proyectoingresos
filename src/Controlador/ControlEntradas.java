package Controlador;

import Modelo.ModeloEntradas;


public class ControlEntradas {

    public void buscar(ModeloEntradas entradas) throws Exception{
        if(entradas == null){
            throw new Exception("No hay datos a buscar, por favor ingrese todos los datos");
        }
        if(entradas.getCedula() == 0){
            throw new Exception("El documento es un dato obligatorio");
        }
    } 
}
